.. index:: Installation
.. index:: Git repository

Getting started
***************

:program:`d`ynasor` is hosted on `gitlab
<dynasor.gitlab.io/dynasor>`_. The full sources (including this
documentation) can be retrieved e.g., via the command line using `git`
::
	   
   git clone git@gitlab.com:dynasor/dynasor.git

or in the form of a tarball or zip-archive.
   
   
.. index:: Compiling; Program

Quickstart
==========

:program:`dynasor` requires :program:`Python` 2.6/2.7 or higher,
:program:`numpy`, as well as a C99-complient C-compiler. On nix
systems, compilation is usually staightforward and merely requires
executing the following command on the command line::

  ./setup.py install --prefix=<build-path>

In case the compilation fails, one should check the settings in the
file `build_config.py`.
   
After a successful compilation the binary can be found in
1<build-path>/bin` whereas the libraries are located in
`<build-path>/lib`. One now ought to update the PATH environment
variable::

   export PATH=<build-path>/bin:$PATH

as well as the PYTHONPATH variable::
     
   export PYTHONPATH=<build-path>/lib/pythonX.Y/site-packages:$PYTHONPATH

where `X.Y` denotes the :program:`Python` version. If
:program:`dynasor` is to used repeatedly it is a good idea to these
commands to the `.bashrc` file (assuming a Bourne again shell).


.. index:: Compiling; Documentation

Documentation
=============

The present documentation is written in reStructuredText and has been
compiled using `sphinx
<http://www.sphinx-doc.org/en/stable/install.html>`_. The source files
can be found in the `doc` subdirectory. In order to build the
documentation (in html format) locally run ::

  sphinx-apidoc -e -o doc/ dsf/
  sphinx-build doc/ userguide/

After successful completion the directory `userguide/` will contain
the html output files. In order for the compilation to successfully
include all components :program:`dynasor` should have been compiled as
described above, including setting the necessary environment
variables.

It should be noted that the documentation requires the sphinx
extension called `sphinxcontrib.bibtex
<http://sphinxcontrib-bibtex.readthedocs.io/en/latest/quickstart.html#installation>`_,
which can be installed e.g., via :program:`pip` ::

  pip install sphinxcontrib-bibtex
