.. raw:: html

  <p>
  <a href="https://gitlab.com/materials-modeling/dynasor/commits/master"><img alt="pipeline status" src="https://gitlab.com/materials-modeling/dynasor/badges/master/pipeline.svg" /></a>
  <a href="https://badge.fury.io/py/dynasor"><img src="https://badge.fury.io/py/dynasor.svg" alt="PyPI version" height="18"></a>
  </p>
  

:program:`dynasor` -- Correlations for everyone
***********************************************

:program:`dynasor` is a tool for calculating total and partial
dynamical structure factors from molecular dynamics (MD)
simulations. The main input consists of a trajectory from a MD
simulation, i.e., a file containing snapshots of the particle
coordinates, and optionally velocities that correspond to
consecutively and equally spaced points in (simulation) time.

:program:`dynasor` and its development are hosted on `gitlab
<https://gitlab.com/materials-modeling/dynasor>`_. Bugs and feature
requests are ideally submitted via the `gitlab issue tracker
<https://gitlab.com/materials-modeling/dynasor/issues>`_. The development
team can also be reached by email via dynasor@materialsmodeling.org.

.. toctree::
   :maxdepth: 2
   :caption: Contents

   introduction
   installation
   theory
   interface_cmdline
   interface_python
   examples/index
   implementation	      
   notes
   credits
   bibliography
   genindex

**dynasor** has been developed at Chalmers University of Technology in
Gothenburg, Sweden, in the
`Materials and Surface Theory division <http://www.materialsmodeling.org>`_
at the Department of Physics.
