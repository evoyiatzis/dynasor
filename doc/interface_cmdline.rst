.. _interface_cmdline:

.. index:: Interface; command line

Command line interface
**********************

:program:`dynasor` expects a number of input parameters. To show all
available options run ::

   dynasor --help

The parameters are grouped in the categories related to input and
output files, **q**-space sampling, as well as time sampling.


.. index:: Parameters; input files
.. index:: Parameters; output files

Input and output files
----------------------

:program:`dynasor` can be run by simply passing a trajectory and an
output file name, using the default values for the sampling parameters
::

   dynasor -f dump.atom --om=output.m --om=output.pickle

where `dump.atom` represens the name of the trajectory file.
:program:`dynasor` has native support for parsing trajectories in
`lammps` dump format :cite:`LAMMPSweb`. If libgmx (gromacs lib) is
available, :program:`dynasor` can use it to read :program:`gromacs`
xtc-files.  If :program:`VMD` is available, :program:`dynasor` can
employ the :program:`VMD` molfileplugin to read other formats (with
some limitations) as well.

Output can be written in standard
:program:`matlab`/:program:`octave`-stype .m-file format and/or in the
form of :program:`Python` pickle-files.

If the system under study contains multiple species, one needs to pass
an index file to :program:`dynasor` or else all atoms will be treated
as identical. The :ref:`molten sodium chloride example
<example_sodium_chloride>` demonstrates this feature and should be
consulted as an example for the use of an index file. For example, in
the case of a small system comtaining eight water molecules one could
employ the following index file ::

	 [ Hydrogen ]
	 2 5 8 11 14 17 20 23
	 3 6 9 12 15 18 21 24
	 [ Oxygen ]
	 1 4 7 10 13 16 19 22

Here, the numbers following each tag (`[...]`) specify the atoms that
belong to the respective species.


.. index:: Parameters; q-space sampling

q-space sampling parameters
---------------------------

The default **q**-point sampling style is isotropic. Using this style
one can specify `--k-max` and `--k-bins`, which determine the
spherical cutoff in **q**-space and the number of radial bins,
respectively. Note that `kmax` is given in units of inverse
nanometers.

To sample along a specific path in **q**-space one can run ::

   dynasor ... --k-sampling="line" --k-direction=10,0,0 --k-points=1000

where the **q**-direction is given in inverse nanometers and is the end point of the path starting from :math:`\Gamma = [0,0,0]`. The `k-points` option specifies the number of **q**-points along the path.


.. index:: Parameters; time sampling

Time sampling parameters
------------------------

The two main time sampling parameters are time window (`--nt`) and
maximum number of frames (`--dt`). The time window (`--nt`) is the
longest time for which correlations are computed and is given in
number of frames. The maximum number of frames (`--max-frames`) sets a
limit on how many frames are read from the trajectroy file. For
example the command ::

	dynasor ... --nt=500 --max-frames=2000

.. figure:: _static/window_sampling.png
    :scale: 30 %
    :align: center

implies that correlation functions are calculated for frames separated
by maximum of 500 frames. This window is moved over the trajectory until it 
reaches frame number max-frames, if no max-frames is specified the whole 
trajecotry will be sampled.

It is also possible to explicitly set the time between two frames
using the `--dt` option in order to get everything in the correct
units.



.. index:: Output options

Output options
--------------

:program:`dynasor` can provide the following quantities

* Partial van Hove function :math:`G(r,t)`
* Partial intermediate scattering function :math:`F(k,t)`
* Partial dynamical structure factor :math:`S(k,\omega)`
* Longitudinal and transversal partial current correlations :math:`C(k,t)`
* Longitudinal and transversal partial current correlations :math:`C(k,\omega)`
* Sampling along specific k-direction
* Self part of :math:`F(k,t)` and :math:`S(k,\omega)` ,

Their exact definitions and relations can be found in the :ref:`theory
section <theory>`. The self part for each of these functions can be
calculated by adding the `--calculate-self` option ::

    dynasor ... --calculate-self






