# Base image
FROM ubuntu:18.04

# Install required packages
RUN apt update -qy
RUN apt upgrade -qy
RUN apt install -qy tzdata
RUN apt install -qy python3-dev \
                    python3-pip \
                    python3-numpy \
                    python3-scipy \
                    python3-h5py \
                    python3-sphinx \
                    pyflakes \
                    pep8 \
                    graphviz

# Set up some Python3 packages via pip
RUN pip3 install --upgrade pip
RUN pip3 install --upgrade setuptools
RUN pip3 install ase \
                 coverage \
                 flake8 \
                 sphinx-rtd-theme \
                 sphinxcontrib-bibtex \
		 sphinx_autodoc_typehints \
                 sphinx_sitemap

